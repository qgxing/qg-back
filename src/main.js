import Vue from "vue";

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "@/styles/index.scss"; // global css

import locale from "element-ui/lib/locale/lang/zh-CN"; // lang i18n

import App from "./App.vue";
import router from "./router/router";
import store from "./store";
import "@/icons"; //icon
import "./registerServiceWorker";
import "./router/permission";

Vue.config.productionTip = false;

Vue.use(ElementUI, { locale });
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
