import { getToken, setToken, removeToken } from "@/utils/auth";

const getName = ()=> {
  let userInfo = JSON.parse(localStorage.getItem("userInfo"))
  return userInfo.userInfo.userName
}

const user = {
  state: {
    name: getName(),
    userInfo: "",
    token: getToken(),
    status: "",
    avatar: ""
  },
  mutations: {
    SET_USER_INFO: (state, userInfo) => {
      state.userInfo = userInfo;
    },
    LogOut: state => {
      state.userInfo = "";
    },
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_STATUS: (state, status) => {
      state.status = status;
    },
    SET_NAME : (state, userName) => {
      state.name = userName;
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar;
    }
  },

  actions: {
    // 用户名登录
    LoginByUsername({ dispatch, commit }, data) {
      commit("SET_USER_INFO", data.userInfo);
      commit("SET_NAME", data.userInfo.userName);
      commit("SET_TOKEN", data.authKey);
      dispatch("setMenuTree", data.userInfo.menuTree);
      setToken(data.authKey);
    },
    getUserInfo(state) {
      return new Promise(resolve => {
        resolve(state.userInfo);
      })
    },
    // 登出
    LogOut({ dispatch,commit }) {
      commit("LogOut");
      dispatch("removeMenu")
      removeToken();
      localStorage.removeItem("authKey");
      localStorage.removeItem("menu");
      localStorage.removeItem("userInfo");
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit("SET_TOKEN", "");
        removeToken();
        resolve();
      });
    }
  }
};

export default user;
