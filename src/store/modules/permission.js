const permission = {
  state: {
    menuTree: localStorage.getItem("menu")
  },

  mutations: {
    SET_MENU_TREE: (state, menuTree) => {
      state.menuTree = menuTree;
    },
    REMOVE_MENU_TREE: state =>{
      state.menuTree = []
    }
  },

  actions: {
    // 左侧菜单树
    setMenuTree({ commit }, menuTree) {
      commit("SET_MENU_TREE", JSON.stringify(menuTree));
      localStorage.setItem("menu", JSON.stringify(menuTree));
    },
    removeMenu({ commit }) {
      commit("REMOVE_MENU_TREE");
    }
  }
};

export default permission;
