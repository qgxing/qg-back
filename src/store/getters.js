const getters = {
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  name: state => state.user.name,
  token: state => state.user.token,
  menuTree: state => state.permission.menuTree ? JSON.parse(state.permission.menuTree) : []
}
export default getters;
