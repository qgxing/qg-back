import fetch from "@/utils/fetch";

/**
 * 登录
 */
export const login = (userName, password) => {
  return fetch("POST", "/admin/login", {
    loginType: "0",
    userName,
    password
  });
};

/**
 * 注销
 * @param {string} authkey
 */
export const logout = authkey => {
  return fetch("POST", "/admin/logout", {
    authkey
  });
};
