import fetch from "@/utils/fetch";

//管理用户


/**
 * 用户分页
 */
export const getUserPage = req => {
  return fetch("POST", "/admin/user/a/page", {
    ...req
  });
};

/**
 * 修改用户
 */
export const modifyUser = form => {
  return fetch("POST", "/admin/user/a/modify", {
    ...form
  });
};

/**
 * 修改用户
 */
export const addUser = form => {
  return fetch("POST", "/admin/user/a/add", {
    ...form
  });
};

/**
 * 删除用户
 */
export const delUser = id => {
  return fetch("GET", `/admin/user/a/delete/${id}`);
};

/**
 * 加载用户角色树
 */
export const loadRoleTree = () => {
  return fetch("POST", "/admin/role/a/tree",{});
};

/**
 * 修改用户角色
 */
export const modifyRole = req => {
  return fetch("POST", "/admin/role/a/access", req);
};

/**
 * 修改角色菜单权限
 */
export const modifyRoleMenu = req => {
  return fetch("POST", "/admin/role/a/modify", req);
};

/**
 * 获取菜单树
 */
export const loadMenuTree = () => {
  return fetch("GET", "/admin/menu/a/tree", {});
};

/**
 * 删除菜单
 */
export const delMenu = id => {
  return fetch("GET", `/admin/menu/a/delete/${id}`);
};

/**
 * 修改菜单
 */
export const modifyMenu = form => {
  return fetch("POST", "/admin/menu/a/modify", {
    ...form
  });
};

/**
 * 添加菜单
 */
export const addMenu = form => {
  return fetch("POST", "/admin/menu/a/add", {
    ...form
  });
};
