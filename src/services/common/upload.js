import axios from "axios";

/**
 * 登录
 */
export const upload = formdata => {
  return new Promise((resolve, reject) => {
    axios.post(
        "https://wxgcd.hengan.com:8765/api/common/attachment/upload",
        formdata
      ).then(rep => {
        resolve(rep);
      }).catch(e => reject(e));
  });
};
