import fetch from "@/utils/fetch";

//普通用户


/**
 * 用户分页
 */
export const getUserPage = req => {
    return fetch("POST", "/admin/userInfo/a/page", {
      ...req
    });
  };
  
  /**
   * 修改用户
   */
  export const modifyUser = form => {
    return fetch("POST", "/admin/userInfo/a/modify", {
      ...form
    });
  };
  
  /**
   * 修改用户
   */
  export const addUser = form => {
    return fetch("POST", "/admin/userInfo/a/add", {
      ...form
    });
  };
  
  /**
   * 删除用户
   */
  export const delUser = id => {
    return fetch("GET", `/admin/userInfo/a/delete/${id}`);
  };