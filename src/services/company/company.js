import fetch from "@/utils/fetch";

/**
 * 单位分页
 */
export const getCompanyPage = req => {
  return fetch("POST", "/admin/company/a/searchCompany", {
    ...req
  });
};
/**
 * 单位树
 */
export const getAllCompany = req => {
  return fetch("POST", "/admin/company/a/selectAllCompanny", {
    ...req
  });
};



/**
 * 添加/编辑单位
 */
export const addCompany = req => {
    return fetch("POST", "/admin/company/a/addCompany", {
      ...req
    });
  };

/**
 * 添加/编辑部门
 */
export const addDepartment = req => {
    return fetch("POST", "/admin/company/a/addDepartment", {
      ...req
    });
  };

/**
 * 添加/编辑岗位
 */
export const addJob = req => {
    return fetch("POST", "/admin/company/a/addJob", {
      ...req
    });
  };
