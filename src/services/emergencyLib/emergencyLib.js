import fetch from "@/utils/fetch";

/**
 * 档案分页
 */
export const getLibPage = req => {
    return fetch("POST", "/admin/emergencyLib/a/page", {
        ...req
    });
};

/**
 * 添加/编辑单档案
 */
export const addLib = req => {
    return fetch("POST", "/admin/emergencyLib/a/add", {
        ...req
    });
};

/**
* 删除菜单
*/
export const delLib = id => {
    return fetch("GET", `/admin/emergencyLib/a/delete/${id}`);
};