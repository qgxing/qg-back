import Vue from "vue";
import Router from "vue-router";
import Layout from "@/views/layout/Layout";

Vue.use(Router);

const constantRouterMap = [

  {
    path: "/redirect",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/redirect/:path*",
        component: () => import("@/views/redirect/index")
      }
    ]
  },
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true
  },
  {
    path: "/404",
    component: () => import("@/views/errorPage/404"),
    hidden: true
  },
  {
    path: "/401",
    component: () => import("@/views/errorPage/401"),
    hidden: true
  },
  //
  {
    path: "",
    component: Layout,
    redirect: "home",
    children: [
      {
        path: "home",
        name: "home",
        component: () => import("@/views/welcome"),
        meta: { title: "管理后台首页", icon: "home", noCache: true }
      }
    ]
  },

  {
    path: "/authority",
    component: Layout,
    redirect: "noredirect",
    alwaysShow: true,
    name: "authorityManage",
    meta: {
      title: "权限管理",
      icon: "management"
    },
    children: [
      {
        path: "menu-tree",
        component: () => import("@/views/authority/MenuTree"),
        name: "menuTree",
        meta: {
          title: "菜单管理",
          icon: "menu",
          noCache: true
        }
      },
      {
        path: "role",
        component: () => import("@/views/authority/RoleTree"),
        name: "roleManage",
        meta: {
          title: "角色管理",
          icon: "role",
          noCache: true
        }
      },
      {
        path: "user",
        component: () => import("@/views/authority/User"),
        name: "userManage",
        meta: {
          title: "用户管理",
          icon: "user",
          noCache: true
        }
      }
    ]
  },
  {
    path: "/person",
    component: Layout,
    redirect: "noredirect",
    alwaysShow: true,
    name: "person",
    meta: {
      title: "人员维护",
      icon: "management"
    },
    children: [
      {
        path: "company",
        component: () => import("@/views/person/Company"),
        name: "company",
        meta: {
          title: "单位信息维护",
          icon: "menu",
          noCache: true
        }
      },
      {
        path: "list",
        component: () => import("@/views/person/PersonList"),
        name: "personList",
        meta: {
          title: "人员维护",
          icon: "menu",
          noCache: true
        }
      },
    ]
  },
  {
    path: "/emergency",
    component: Layout,
    redirect: "noredirect",
    alwaysShow: true,
    name: "emergency",
    meta: {
      title: "应急库管理",
      icon: "management"
    },
    children: [
      {
        path: "record",
        component: () => import("@/views/emergency/RecordList"),
        name: "record",
        meta: {
          title: "档案管理",
          icon: "menu",
          noCache: true
        }
      },
      {
        path: "expert",
        component: () => import("@/views/emergency/RecordList"),
        name: "expert",
        meta: {
          title: "专家库",
          icon: "menu",
          noCache: true
        }
      },
      {
        path: "rescueTeam",
        component: () => import("@/views/emergency/RecordList"),
        name: "rescueTeam",
        meta: {
          title: "救援队伍",
          icon: "menu",
          noCache: true
        }
      },
    ]
  },
  { path: "*", redirect: "/404", hidden: true }
];

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: constantRouterMap
});

export { constantRouterMap };
