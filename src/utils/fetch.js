/* eslint-disable no-console */
import axios from "axios";
import {Message,Loading} from 'element-ui'
// const qs = require("qs");
//自动切换环境
if (process.env.NODE_ENV == "development") {
  axios.defaults.baseURL = "http://localhost:7777/api";
  // axios.defaults.baseURL = "http://193.112.66.61:7777/api";
} else if (process.env.NODE_ENV == "debug") {
  axios.defaults.baseURL = "/api";
} else if (process.env.NODE_ENV == "production") {
  axios.defaults.baseURL = "http://***********/";
}
//设置超时时间
axios.defaults.timeout = 10000;
axios.defaults.headers["Content-Type"] = "application/json";

const fetch = async (type, url, params) => {
  switch (type) {
    case "POST":
      try {
        Loading.service({text:"Loading..."});
        let res = await axios.post(url, JSON.stringify(params));
        Loading.service().close();
        console.log(res);
        res = res.data;
        return new Promise((resolve, reject) => {
          if (res.code === 200) {
            resolve(res);
          } else {
            reject(res);
          }
        });
      } catch (err) {
        // return (e.message)
        Loading.service().close();
        Message.error("服务器出错~~");
        console.log(err);
      }
      break;
    case "GET":
      try {
        Loading.service({text:"Loading..."});
        let res = await axios.get(url, { params: params });
        Loading.service().close();
        res = res.data;
        return new Promise(resolve => {
          if (res.code === 200) {
            resolve(res);
          } else {
            resolve(res);
          }
        });
      } catch (err) {
        Loading.service().close();
        Message.error("服务器出错~~");
        console.log(err);
      }
      break;
    default:
      break;
  }
};
export default fetch;
