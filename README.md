

# qg-back

#### 介绍
后台管理搭建模板 VueCli3

#### 软件架构
快速搭建后台管理


#### 安装教程


 ### Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
-------
Edited in 2019.06



